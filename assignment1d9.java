import java.sql.*;
import java.util.Scanner;

public class assignment1d10 {
    static int pilihan, pilihan2;
    static Statement stmt;
    static int inputID;
    static String inputNama;
    static int inputUmur;
    static ResultSet rs;
    static String inputNamaUpdate;
    static int inputUmurUpdate;
    static int inputIDUpdate;
    static int inputIDDelete;

    public static void selectData() throws SQLException {
        ResultSet rs = stmt.executeQuery("select * from emp");
        while (rs.next())
            System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getString(3));
    }

    public static void inputData () throws SQLException {
        for (int i = 0; i < pilihan2; i++) {
            stmt.executeUpdate("insert into emp (name, age) values ('" + inputNama + "', " + inputUmur + ")");
        }
    }

    public static void updateData () throws SQLException {
        stmt.executeUpdate("UPDATE emp\n SET name = '"+inputNamaUpdate+"', age = '"+inputUmurUpdate+"' WHERE id = '"+inputIDUpdate+"'");

    }

    public static void deleteData() throws SQLException {
        stmt.executeUpdate("DELETE FROM emp WHERE id = '"+inputIDDelete+"';");
    }


    public static void main(String args[]) throws ClassNotFoundException, SQLException {
        Scanner input = new Scanner(System.in);

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo", "root", "anisa2000");

            Statement stmt = con.createStatement();

            do {
                System.out.println("MENU UTAMA");
                System.out.println("1. Show All Record");
                System.out.println("2. Insert Record");
                System.out.println("3. Update Record");
                System.out.println("4. Delete Record");
                System.out.println("99. EXIT");
                System.out.print("Masukkan pilihanmu (1/2/3/4/99): ");
                pilihan = input.nextInt();


                switch (pilihan) {
                    case 1:
                        selectData();
                        break;
                    case 2:
                        System.out.print("Berapa data yang ingin anda masukkan: ");
                        int pilihan2 = input.nextInt();

                        System.out.print("Nama: ");
                        String inputNama = input.next();
                        System.out.print("Umur: ");
                        int inputUmur = input.nextInt();

                        inputData();
                        selectData();
                        break;

                    case 3:
                        System.out.print("Input ID: ");
                        int inputIDUpdate = input.nextInt();

                        System.out.print("Nama: ");
                        inputNamaUpdate = input.next();
                        System.out.print("Umur: ");
                        inputUmurUpdate = input.nextInt();

                        updateData();
                        selectData();
                        break;

                    case 4:
                        System.out.println("Input ID: ");
                        inputIDDelete = input.nextInt();

                        deleteData();
                        selectData();
                        break;

                    default:
                        if (pilihan == 99) {
                            con.close();
                            break;
                        } else {
                            System.out.println("Menu yang anda pilih tidak tersedia");
                        }
                }

                } while (pilihan != 99);

            } catch (Exception e) {
            System.out.println(e);
        }
    }

}