import java.sql.*;
import java.util.Scanner;

public class assignment2d10 {

    static int pilihan, pilihan2;
    static Statement stmt;
    static int inputID;
    static String inputNama;
    static int inputUmur;
    static ResultSet rs;
    static String inputNamaUpdate;
    static int inputUmurUpdate;
    static int inputIDUpdate;
    static int inputIDDelete;
    static Connection con;
    static PreparedStatement prepStmt;

        public static void selectData() throws SQLException {
            ResultSet rs = stmt.executeQuery("select * from emp");
            while (rs.next())
                System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getString(3));
        }

        public static void inputData () throws SQLException {
            for (int i = 0; i < pilihan2; i++) {

                prepStmt=con.prepareStatement("insert into emp (name,age) values (?,?)");
                prepStmt.setString(1,inputNama);
                prepStmt.setInt(2,inputUmur);

            }
            int j=prepStmt.executeUpdate();
            System.out.println("======"+ j +" records inserted======");
        }

        public static void updateData () throws SQLException {

            prepStmt=con.prepareStatement("UPDATE emp\n SET name =?, age =? WHERE id = ?");
            prepStmt.setString(1,inputNamaUpdate);//1 specifies the first parameter in the query
            prepStmt.setInt(2,inputUmurUpdate);
            prepStmt.setInt(3,inputIDUpdate);

            int j=prepStmt.executeUpdate();
            System.out.println("======"+ j +" records updated======");

        }

        public static void deleteData() throws SQLException {

            prepStmt=con.prepareStatement("DELETE FROM emp WHERE id =?;");
            prepStmt.setInt(1,inputIDDelete);

            int j=prepStmt.executeUpdate();
            System.out.println("======"+ j +" records deleted======");

        }


        public static void main(String args[]) throws ClassNotFoundException, SQLException {
            Scanner input = new Scanner(System.in);

            try {
                Class.forName("com.mysql.cj.jdbc.Driver");

                con = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/sonoo", "root", "anisa2000");

                stmt = con.createStatement();

                do {
                    System.out.println("\nMENU UTAMA");
                    System.out.println("1. Show All Record");
                    System.out.println("2. Insert Record");
                    System.out.println("3. Update Record");
                    System.out.println("4. Delete Record");
                    System.out.println("99. EXIT");
                    System.out.print("Masukkan pilihanmu (1/2/3/4/99): ");
                    pilihan = input.nextInt();


                    switch (pilihan) {
                        case 1:
                            selectData();
                            break;
                        case 2:
                            System.out.print("Berapa data yang ingin anda masukkan: ");
                            pilihan2 = input.nextInt();

                            System.out.print("Nama: ");
                            inputNama = input.next();
                            System.out.print("Umur: ");
                            inputUmur = input.nextInt();

                            inputData();
                            selectData();
                            break;

                        case 3:
                            System.out.print("Input ID: ");
                            inputIDUpdate = input.nextInt();

                            System.out.print("Nama: ");
                            inputNamaUpdate = input.next();
                            System.out.print("Umur: ");
                            inputUmurUpdate = input.nextInt();

                            updateData();
                            selectData();
                            break;

                        case 4:
                            System.out.print("Input ID: ");
                            inputIDDelete = input.nextInt();

                            deleteData();
                            selectData();
                            break;

                        default:
                            if (pilihan == 99) {
                                con.close();
                                break;
                            } else {
                                System.out.println("Menu yang anda pilih tidak tersedia");
                            }
                    }

                } while (pilihan != 99);

            } catch (Exception e) {
                System.out.println(e);
            }


    }

}
